// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB1YloM-BMpOZLkDfMR16fxTeJmDm2zwp4",
  authDomain: "devcamp-r25.firebaseapp.com",
  projectId: "devcamp-r25",
  storageBucket: "devcamp-r25.appspot.com",
  messagingSenderId: "482009833848",
  appId: "1:482009833848:web:5548bd7a9d175e9804e88d"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;