import logo from './logo.svg';
import './App.css';

import { GoogleAuthProvider, signInWithPopup, signOut, onAuthStateChanged } from "firebase/auth";

import auth from "./firebase";
import { useEffect, useState } from 'react';

const provider = new GoogleAuthProvider();

function App() {
  const [user, setUser] = useState(null);

  const loginGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        console.log(result);
        setUser(result.user);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  const logoutGoogle = () => {
    signOut(auth)
      .then(() => {
        setUser(null);
      })
      .catch((error) => {
        console.error(error);
      })
  }
  
  useEffect(() => {
    onAuthStateChanged(auth, (result) => {
      if (result) {
        setUser(result);
      } else {
        setUser(null);
      }
    })
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        {
          user ?
          <>  
            <h4>Hi, {user.displayName}</h4>
            <img src={user.photoURL} style={{width: "50px", borderRadius: "50%"}} alt="avatar"></img>
            <br></br>
            <button onClick={logoutGoogle}>Sign out</button>
          </>
          :
          <button onClick={loginGoogle}>Sign in with Google</button>
        }
       
      </header>
    </div>
  );
}

export default App;
